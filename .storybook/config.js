import * as React from 'react';
import { addDecorator, addParameters } from '@storybook/react';
import { themes } from '@storybook/theming';

import '../src/styles/tailwind.css';

const withPadding = story => <div className="p-4">{story()}</div>

addDecorator(withPadding);


addParameters({
  options: {
    name: 'FastFood', 
    theme: themes.light
  },
})



import * as React from 'react'; 
import { any } from 'prop-types';

import {toRupiah} from '../../utils/to-rupiah';
import './style.css';

export default function CardFood({item, onAdd}){
   let {imgSrc, title, price} = item;
   return (
     <div className="relative card-food pt-10 px-2">
       <div className="card-food_bg p-5 text-white rounded-lg">
         <div className="absolute left-0 w-full card-food_img ">
           <img className="mx-auto" src={imgSrc} alt={title} width="120" />
         </div>
         <div className="flex justify-between mt-16">
           <div className="text-sm">
             <div>{title}</div>
             <div>{toRupiah(price)}</div>
           </div>
           <div className="self-center">
             <button
               className="bg-darklighter rounded-full text-burger text-center h-6 w-6 text-sm align-middle shadow-lg"
               onClick={_ => onAdd(item)}
             >+</button>
           </div>
         </div>
       </div>
     </div>
   )
}

CardFood.defaultProps = {
   
}

CardFood.propTypes = {
}

import * as React from 'react'; 
import { any } from 'prop-types';

import CardBasket from '../CardBasket';
import { toRupiah } from '../../utils/to-rupiah';

export default function BasketBox({items, onInc, onDec}){
   const subTotal = items.reduce((sum, item) => {sum += (item.price * item.qty); return sum},0)
   return (
     <div className="w-full p-5 bg-dark rounded">

       <div className="font-bold text-xl text-white">Order Summary</div>
       <hr className="border-gray-800 my-5"/>

       {items.map((item, index) => {
          return <div key={index} className="my-2">
            <CardBasket item={item} onInc={onInc} onDec={onDec}/>
          </div>
       })}

       <hr className="border-gray-800 my-5"/>

       <div className="text-sm text-white text-gray-500">
         <div>
          {items.length} item(s)
         </div>
         <div className="flex justify-between">
           <div>Subtotal</div>
           <div>{toRupiah(subTotal)}</div>
         </div>
         <div className="flex justify-between">
           <div>Tax</div>
           <div>{toRupiah(subTotal * 0.1)}</div>
         </div>

         <div className="flex justify-between mt-5">
           <div className="text-xl">Total</div>
           <div className="font-bold">{toRupiah(subTotal + (subTotal * 0.1))}</div>
         </div>
       </div>

       <div className="flex justify-between mt-5">
         <button className="border border-burger rounded py-1 px-5 text-burger">
           Hold Order
         </button>
         <button className="bg-burger py-1 px-5 rounded text-dark">
           Checkout
         </button>
       </div>
       
     </div>
   )
}

BasketBox.defaultProps = {
   
}

BasketBox.propTypes = {
  items: any,
}

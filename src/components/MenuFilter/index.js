import * as React from 'react'; 
import { any } from 'prop-types';
import "./style.css";

export default function MenuFilter({menus, active, onChange}){
   return (
     <div className="flex text-gray-400 bg-darker p-2 text-sm overflow-auto menu-filter">
       <div className={`px-5 transition-colors cursor-pointer mr-4 ${!active ? 'bg-red-600 text-white rounded-lg' : ''}`} onClick={_ => onChange(null)}>All</div>
       {menus.map((menu, index) => {
         return <div key={index} className={`transition-colors duration-500 cursor-pointer pb-1 ${active == menu ? 'bg-red-600 text-white rounded-lg' : ''} px-5 mr-4`} onClick={_ => onChange(menu)}>
             {menu}
          </div>
       })}
     </div>
   )
}

MenuFilter.defaultProps = {
   
}

MenuFilter.propTypes = {
  menus: any,
  active: any,
  onChange: any,
}

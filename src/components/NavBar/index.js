import * as React from 'react'; 
import { shape, string, func  } from 'prop-types';

import './style.css'

export default function NavBar({menus, active, onChange}){
   return (
     <div className={`bg-darkest py-2 w-16 text-center navbar h-full flex flex-col md:justify-center h-screen`}>
       {menus.map((menu, index) => {
         return <div 
           key={index} 
           className={`transition duration-500 ease-in-out my-5 navbar_item ${active == menu.key ? 'text-burgerdarker' : 'text-gray-400'} cursor-pointer`}
           onClick={_ => onChange(menu.key)}
         >
           <img src={active == menu.key ? menu.icon.replace('.png', '-active.png') : menu.icon} alt={menu.label} className="mx-auto" />
            <div>
              {menu.label}
            </div>
          </div>
       })}
     </div>
   )
}

NavBar.defaultProps = {
   
}

NavBar.propTypes = {
  menus: shape({
     key: string.isRequired,
     icon: string.isRequired, 
     label: string.isRequired
  }),
  active: string, 
  onChange: func
}

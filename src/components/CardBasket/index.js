import * as React from 'react'; 
import { any } from 'prop-types';
import { toRupiah } from '../../utils/to-rupiah';

export default function CardBasket({item, onInc, onDec}){
  let {price, qty, title, imgSrc} = item;
   return (
     <div className="bg-darklight p-2 rounded text-gray-200 flex card-burger">
       <div className="pr-4">
         <img src={imgSrc} alt={title} width="60" className="shadow-lg"/>
       </div>
       <div className="flex-1">
         <div className="card-burger_title mb-2">{title}</div>
         <div className="card-burger_qty flex">
            <button 
              className="bg-darklighter text-burger p-0 px-2 rounded shadow-lg"
              onClick={_ => onDec(item)}
            >-</button>
            <input type="text" value={qty} className="bg-darklight w-10 text-center" />
            <button 
              className="bg-darklighter text-burger p-0 px-2 rounded shadow-lg"
              onClick={_ => onInc(item)}
            >+</button>
         </div>
       </div>
       <div className="self-center">
         <div className="card-burger">@{toRupiah(price).replace('.000,00', 'rb')}</div>
       </div>
     </div>
   )
}

CardBasket.defaultProps = {
  onInc: () => {},
  onDec: () => {}
}


// Jangan tanya kenapa ini any semua, namanya juga prototype doank
CardBasket.propTypes = {
  price: any,
  qty: any,
  title: any,
  imgSrc: any,
  onInc: any,
  onDec: any,
}

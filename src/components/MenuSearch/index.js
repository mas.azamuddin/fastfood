import * as React from 'react'; 
import { any, string, func } from 'prop-types';

import './style.css';

export default function MenuSearch({keyword, onChange}){
   const [focus, setFocus] = React.useState(false);

   const handleFocus = function(){
     setFocus(true); 
   }

   const handleBlur = function(){
     setFocus(false)
   }

   return (
     <div className={`transition-all mx-auto duration-500 ease-in-out flex text-white justify-between bg-darklight ${!focus ? 'w-64' : 'w-full'} menu-filter`}>
       <input 
         type="text" 
         className="pl-4 py-1 bg-transparent" 
         placeholder="Search menu" 
         value={keyword ? keyword : ''}
         onFocus={handleFocus} 
         onBlur={handleBlur}
         onChange={e => onChange(e.target.value)}
       />
       <button className="flex-shrink-0 text-white px-2">
         <img src="assets/icons/search.png" alt="?" width="32" />
       </button>
     </div>
   )
}

MenuSearch.defaultProps = {
   
}

MenuSearch.propTypes = {
  keyword: string,
  onChange: func
}

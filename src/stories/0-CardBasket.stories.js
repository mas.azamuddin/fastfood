import * as React from 'react'; 

import CardBasket from '../components/CardBasket';

export default {
  title: 'CardBasket', 
  component: CardBasket
}

const item = {
   title: 'Burger', 
   price: 23000, 
   qty: 2,
   imgSrc: 'assets/burger.png'
}

export const Default = () => <div className="w-1/3">
  <CardBasket item={item}/>
</div>

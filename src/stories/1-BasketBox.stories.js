import * as React from 'react'; 
import BasketBox from '../components/BasketBox';

export default {
  title: 'BasketBox', 
  component: BasketBox
}

const items = [
  {
   title: 'Burger', 
   price: 23000, 
   qty: 2,
   imgSrc: 'assets/burger.png'
  },
  {
   title: 'Burger Box', 
   price: 23000, 
   qty: 2,
   imgSrc: 'assets/burger.png'
  }

]

export const Default = () => <div className="w-1/3">
  <BasketBox items={items}/>
</div>

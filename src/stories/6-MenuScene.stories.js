import * as React from 'react'; 

import MenuSearch from '../components/MenuSearch';
import MenuFilter from '../components/MenuFilter';
import CardFood from '../components/CardFood';
import BasketBox from '../components/BasketBox';
import NavBar from '../components/NavBar';


export default {
  title: 'MenuScene', 
}

const navbarItems = [
  {
    key: 'burger', 
    icon: 'assets/icons/burger.png', 
    label: 'Burger'
  }, 
  {
    key: 'pizza', 
    icon: 'assets/icons/pizza.png', 
    label: 'pizza',
  },
  {
    key: 'chicken', 
    icon: 'assets/icons/chicken.png', 
    label: 'chicken',
  }, 
  {
    key: 'fries', 
    icon: 'assets/icons/fries.png', 
    label: 'snack',
  },
  {
    key: 'drink', 
    icon: 'assets/icons/drink.png', 
    label: 'beverage'
  }
]


const items = [
  {
   key: 'burger',
   title: 'Burger', price: 23000, 
   imgSrc: 'assets/burger.png',
   category: 'burger',  
   types: ['Tomato']
  },
  {
   key: 'burger-box',
   title: 'Burger Box', 
   price: 23000, 
   imgSrc: 'assets/burger.png',
   category: 'burger',
   types: ['Tomato', 'Cheese']
  },
  {
   key: 'burger-cheese',
   title: 'Burger Cheese', 
   price: 23000, 
   imgSrc: 'assets/burger.png',
   category: 'burger',
    types: ['Cheese']
  },
  {
   key: 'burger-chicken',
   title: 'Burger Chicken', 
   price: 23000, 
   imgSrc: 'assets/burger.png',
   category: 'burger', 
   types: ['Chicken']
  },
  {
   key: 'burger-cheese-extra',
   title: 'Burger Cheese Extra', 
   price: 23000, 
   imgSrc: 'assets/burger.png',
   category: 'burger',
   types: ['Cheese']
  },
  {
   key: 'pizza-a',
   title: 'Pizza Original', 
   price: 43000, 
   imgSrc: 'assets/pizza.png',
   category: 'pizza',
    types: ['Original']
  },
  {
   key: 'pizza-cheese',
   title: 'Pizza Cheese', 
   price: 43000, 
   imgSrc: 'assets/pizza.png',
   category: 'pizza',
    type: ['Cheese']
  },
  {
   key: 'pizza-cheese-extra',
   title: 'Pizza Cheese Extra', 
   price: 43000, 
   imgSrc: 'assets/pizza.png',
   category: 'pizza', 
    types: ['Cheese']
  },
  {
   key: 'chicken-a',
   title: 'Chicken Original', 
   price: 43000, 
   imgSrc: 'assets/chicken.png',
   category: 'chicken',
    types: ['Original']
  },
  {
   key: 'chicken-b',
   title: 'Chicken Spicy', 
   price: 43000, 
   imgSrc: 'assets/chicken.png',
   category: 'chicken',
    type: ['Spicy']
  },
  {
   key: 'coffee-a',
   title: 'Americano Hot', 
   price: 43000, 
   imgSrc: 'assets/drink.png',
   category: 'drink',
   types: ['Coffee']
  },
  {
   key: 'fries-a',
   title: 'French Fries', 
   price: 43000, 
   imgSrc: 'assets/fries-3.png',
   category: 'fries',
   types: ['Original']
  },
]

export const Default = () => {
  let [activeNav, setActiveNav] = React.useState('burger');
  let [activeFoodType, setActiveFoodType] = React.useState(null);
  let [cartItems, setCartItems] = React.useState([]);
  let [typesFilter, setTypesFilter] = React.useState(['Tomato', 'Cheese', 'Chicken', 'Beef'])
  let [filterKeyword, setFilterKeyword] = React.useState(null);

  React.useEffect(() => {
    if(filterKeyword){
      setActiveNav(null)
    }
  }, [filterKeyword])

  React.useEffect(() => {
    switch(activeNav){
      case 'burger':
        setTypesFilter(['Tomato', 'Cheese', 'Chicken', 'Beef'])
      break;

      case 'pizza':
        setTypesFilter(['Cheese', 'Original', 'Peperoni'])
        break;

      case 'chicken':
        setTypesFilter(['Original', 'Spicy'])
        break;

      case 'fries':
        setTypesFilter(['Original', 'Spicy', 'Xtra Large'])
        break;

      case 'drink':
        setTypesFilter(['Coffee', 'Soft Drink', 'Juice'])
        break;

      default:
        setTypesFilter([])
    } 

    setFilterKeyword(null);
    setActiveFoodType(null)
  }, [activeNav])
  
  let addToCart = item => {

    if(cartItems.find(i => i.key === item.key)) {
      setCartItems(cartItems.map(itm => itm.key === item.key ? {...itm, qty: itm.qty + 1} : itm))
    } else {
      setCartItems([...cartItems, {...item, qty: 1}])
    }

  }

  let decreaseFromCart = item => {
    let itemExist = cartItems.find(i => i.key === item.key);
    if(itemExist) {
      if(itemExist.qty === 1){
        setCartItems(cartItems.filter(itm => itm.key !== item.key));
      } else {
        setCartItems(cartItems.map(itm => itm.key === item.key ? {...itm, qty: itm.qty - 1} : itm))
      }
    } 
  }

  return <div className="bg-darker rounded flex">
    <div className="item-center bg-darkest">
      <NavBar menus={navbarItems} active={activeNav} onChange={key => setActiveNav(key)}/>
    </div>

    <div className="w-5/6">   
      <div className="topbar px-2 py-5 md:flex justify-between">
        <div className="text-white font-bold text-xl text-center md:text-left mb-5 md:mb-0">
          Fast Food
        </div>
        <div className="flex-grow md:max-w-lg px-2 mb-5 md:mb-0">
          <MenuSearch onChange={keyword => setFilterKeyword(keyword)} keyword={filterKeyword}/>
        </div>
        <div className="text-white text-center md:text-left flex justify-center">
          <img src="assets/icons/invoice.png" width="42"/>
          <img src="assets/icons/gear.png" width="42"/>
          <img src="assets/icons/user.png" width="42"/>
        </div>
      </div>

      <div className="md:flex py-5 w-full">
        <div className="mt-5 w-full md:w-2/3">
          <div className="pl-2">
            <MenuFilter menus={typesFilter} active={activeFoodType} onChange={key => setActiveFoodType(key)}/>
          </div>

          <div className="flex flex-wrap">
            {items
                .filter(i => !filterKeyword ? i.category == activeNav : true)
                .filter(i => activeFoodType && !filterKeyword ? i.types && i.types.indexOf(activeFoodType) > -1 : true)
                .filter(i => filterKeyword && filterKeyword.length ? i.title.toLowerCase().indexOf(filterKeyword.toLowerCase()) > -1 : true)
                .map((item, index) => {
              return <div key={index} className="px-2 w-full md:w-1/3">
                <CardFood item={item} onAdd={item => addToCart(item) }/>
               </div>
            })}
          </div>

        </div>

        <div className="w-full md:w-1/3 mx-0 md:mx-2 px-4 mt-10 md:mt-0 ">
          <BasketBox items={cartItems} onInc={item => addToCart(item)} onDec={item => decreaseFromCart(item)} />
        </div>
      
      </div>

    </div>
  </div>
}

import * as React from 'react'; 
import CardFood from '../components/CardFood';

export default {
   title: 'CardFood', 
   component: CardFood
}

const item = {
   title: 'Burger', 
   price: 23000, 
   qty: 2,
   imgSrc: 'assets/burger.png'
}

export const Default = () => {
  return <div className="w-1/3">
    <CardFood item={item}/>
  </div>
}

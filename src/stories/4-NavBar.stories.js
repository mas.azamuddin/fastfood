import * as React from 'react'; 

import NavBar from '../components/NavBar';

export default {
  title: 'NavBar', 
  component: NavBar
}


const items = [
  {
    key: 'burger', 
    icon: 'assets/icons/burger.png', 
    label: 'Burger'
  }, 
  {
    key: 'pizza', 
    icon: 'assets/icons/pizza.png', 
    label: 'pizza',
  },
  {
    key: 'chicken', 
    icon: 'assets/icons/chicken.png', 
    label: 'chicken',
  }, 
  {
    key: 'snacks', 
    icon: 'assets/icons/fries.png', 
    label: 'snack',
  },
  {
    key: 'drink', 
    icon: 'assets/icons/drink.png', 
    label: 'beverage'
  }
]

export const Default = () => {

  let [active, setActive] = React.useState('burger')

  return <NavBar menus={items} active={active} onChange={key => setActive(key)}  />
}

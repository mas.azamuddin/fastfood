import * as React from 'react';
import MenuFilter from '../components/MenuFilter';

export default {
  title: 'MenuFilter', 
  component: MenuFilter
}


let menus = [
   'Bacon', 
   'Beef', 
   'Cheese', 
   'Chicken',
]

export const Default = () => {
  let [active, setActive] = React.useState(null);

  return <div>
  <MenuFilter menus={menus} active={active} onChange={selected => setActive(selected)}/>
</div>
}
